#include "Stage_Loader.h"



Stage_Loader::Stage_Loader()
	:m_stage_coockie_num(0)
{
}

int Stage_Loader::get_rows_num() const
{
	return m_rows;
}
int Stage_Loader::get_cols_num() const
{
	return m_cols;
}

void Stage_Loader::load_level(std::string level_name, std::unique_ptr <Pacman> &pac,
	std::vector< std::vector < std::unique_ptr <Static> > > *static_vec,
	std::vector < std::unique_ptr <Devil> > *devil_vec)
{
	m_stage_coockie_num = 0;
	std::ifstream levelfile;
	int x, y;
	levelfile.open(level_name);
	char current;
	if (levelfile.is_open())
	{
		
		levelfile >> x >> y;
		levelfile.get(current); // for the first '\n'
	}
	m_rows = y;
	m_cols = x;

	
		static_vec->resize(y);
		for (int i = 0; i < y; ++i)
			static_vec->at(i).resize(x);

	

		for (int i = 0; i < y; i++) // y
		{
			static_vec->push_back({});
			for (int j = 0; j < x; j++) //x
			{
				static_vec->back().push_back({});
			}
		}
	

	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			levelfile.get(current);
			if (current != ' ' && current != '\n')
			{
				if (current == '@' || current == 'W' || current == 'S') // pacman
					update_pacman(pac, current, j, i);
				else
					if (current == '%' || current == 'T' || current == 'G') // devil
						insert_to_dynamic(devil_vec, current,j,i ); // fix j,i
					else												// wall/cookie
						insert_to_static(static_vec, current, j, i); //// fix j,i
					
			}

		}
		levelfile.get(current); // for the '\n'
	}

}

void Stage_Loader::update_pacman(std::unique_ptr <Pacman> &pacman, char type, int x, int y)
{
	sf::Vector2f fixed_location((x * 35.f) + 5.f, (y * 35.f) + 5.f);
	sf::Color color;
	switch (type)
	{
	case '@':
		color = sf::Color::Red;
		break;
	case 'W':
		color = sf::Color::Green;
		break;
	case 'S':
		color = sf::Color::Blue;
		break;

	}
	//pacman = new Pacman(color, fixed_location, type);
	pacman = std::make_unique <Pacman>(color, fixed_location, type);
	pacman->set_position(fixed_location);
	pacman->set_color(color);
	
}
void Stage_Loader::insert_to_dynamic(std::vector < std::unique_ptr <Devil> > *devil_vec, char type, int x, int y)
{
	sf::Vector2f fixed_location(x * 35.f, y * 35.f);
	sf::Color color;
	switch (type)
	{
	case '%':
		color = sf::Color::Red;
		break;
	case 'T':
		color = sf::Color::Green;
		break;
	case 'G':
		color = sf::Color::Blue;
		break;

	}

	int random = rand() % 2;
	//if(random == 0)
		devil_vec->push_back(std::make_unique<Smart_Devil>(color, fixed_location,type));
	//else
		//devil_vec->push_back(std::make_unique<Random_Devil>(color, fixed_location,type));

	
}
void Stage_Loader::insert_to_static(std::vector< std::vector < std::unique_ptr <Static> > > *static_vec, char type, int x, int y)
{
	sf::Vector2f fixed_location(x * 35.f, y * 35.f);
	sf::Color color;
	switch (type)
	{
		// cookie
	case '*':
		color = sf::Color::Red;
		//m_stage_coockie_num++;
		break;
	case 'I':
		color = sf::Color::Green;
		//m_stage_coockie_num++;
		break;
	case 'K':
		color = sf::Color::Blue;
		//m_stage_coockie_num++;
		break;

		// wall
	case '#':
		color = sf::Color::Red;
		break;
	case 'E':
		color = sf::Color::Green;
		break;
	case 'D':
		color = sf::Color::Blue;
		break;
	}

	if (type == '*' || type == 'I' || type == 'K')
	{
		m_stage_coockie_num++;
		static_vec->at(x).at(y) = std::make_unique<Cookie>(color, fixed_location, type);
	}
	if (type == '#' || type == 'E' || type == 'D')
		static_vec->at(x).at(y)= std::make_unique<Wall>(color, fixed_location,type); // need to add the type!

}

int Stage_Loader::get_num_cookies() const
{
	return m_stage_coockie_num;
}

Stage_Loader::~Stage_Loader()
{
}
