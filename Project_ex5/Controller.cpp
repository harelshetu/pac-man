#include "Controller.h"



Controller::Controller()
	:m_current_level(1),m_status(0,3,100,100,false,false)
{}

void Controller::load_level()
{
	m_screen.load_level(m_current_level);
	int num_of_rows = m_screen.get_rows_num();
	int num_of_cols = m_screen.get_cols_num();
	m_window.create(sf::VideoMode::VideoMode(num_of_rows * 35, num_of_cols * 35 + 40), "Pacman");
}

void Controller::run()
{      
	sf::Sound audio_start_game,
				audio_strike,
				audio_game_over;
	audio_start_game.setBuffer(ResourceManager::instance().Get_Start_game_music());
	audio_strike.setBuffer(ResourceManager::instance().Get_Strike_music());
	audio_game_over.setBuffer(ResourceManager::instance().Get_Defeat_music());
			 // audio_eat_cookie,
			  // audio_strike;

	//audio_start_game.setBuffer(ResourceManager::);

	/*if (!start_game.loadFromFile("start_game.ogg"))
		std::cout << "cannot find audio";
	if (!eat_cookie.loadFromFile("eat_cookie.ogg"))
		std::cout << "cannot find audio";
	if (!strike.loadFromFile("strike.ogg"))
		std::cout << "cannot find audio";*/


	/*audio_start_game.setBuffer(start_game);
	audio_eat_cookie.setBuffer(eat_cookie);
	audio_strike.setBuffer(strike);*/



	bool want_to_play;
	want_to_play = m_menu.start(m_window);
	//m_menu.lose(m_window);
	//m_menu.win(m_window);
	
	if (want_to_play)
	{
		load_level();//  load level 1
		audio_start_game.play();
	}


	/*sf::Font david;
	if (!david.loadFromFile("david.ttf"))
		std::cout << "cannot load font";*/

	std::ostringstream s_score,
		s_lives;
	
	sf::Text game_score,
		lives;

	s_score << "Score: " << m_status.get_score();
	s_lives << "Lives: " << m_status.get_lives();


	
	///////////////////////////////////////
	sf::Clock clock;
	sf::Time stopper;
	sf::Clock cookie_clock;
	float lastSec = 0;
	float lastSec2 = 0;
	float currentSec = 0;
	//int pixelPerSecond = 1000;

	int direction = 5;
	int prev_direction = direction;
	//float factor = 100;

	///////////////////////////////////////
	int devil_direction = rand() % 4;
	m_screen.set_devil_direction(devil_direction);
	

	while (m_window.isOpen()) // main loop
	{
		//m_window.clear();
		//m_screen.draw(m_window); // print everything
		

		lives.setCharacterSize(24);
		game_score.setCharacterSize(24);
		lives.setPosition(180, m_screen.get_cols_num() * 35);
		game_score.setPosition(0, m_screen.get_cols_num() * 35);
		game_score.setFont(ResourceManager::instance().GetFont());
		lives.setFont(ResourceManager::instance().GetFont());

		game_score.setString(s_score.str());
		lives.setString(s_lives.str());

		if (m_screen.get_stage_cookies() == m_screen.get_eaten_cookies())
		{
			m_screen.clear_all(); // empty the vectors
			m_current_level++;
			int score = m_status.get_score();
			m_status.set_score(score + (((m_screen.Get_Num_Devils() + 1) * 50)));
			if (m_current_level > 4)
			{
				want_to_play = m_menu.win(m_window);
				if (want_to_play)
				{
					m_status.set_life(3);
					m_status.set_score(0);
					m_screen.set_eaten_cookies(0);
					m_current_level = 1;
					m_screen.clear_all();
					load_level();
					direction = 5;
				}
			}
			else
				load_level(); // load levels 2, 3....
			direction = 5;
			m_status.set_speed(m_status.get_basic_speed());

			devil_direction = rand() % 4;  // delete later?
			m_screen.set_devil_direction(devil_direction); // delete later?
		}

		sf::Event Event;
		while (m_window.pollEvent(Event))
		{
			switch (Event.type)
			{	

			case sf::Event::Resized:
				m_window.setView(sf::View(sf::FloatRect(0, 0, (float)Event.size.width, (float)Event.size.height)));
				break;

			case sf::Event::KeyPressed:
				prev_direction = direction;
				switch (Event.key.code)
				{
					
				case sf::Keyboard::Up: 
					direction = 0;  // change to const
					//m_screen.rotatePacman(270);
					break;
				case sf::Keyboard::Down: 
					direction = 1;
					//m_screen.rotatePacman(90);
					break;
				case sf::Keyboard::Left: 
					direction = 2;
					//m_screen.rotatePacman(180);
					break;
				case sf::Keyboard::Right: 
					direction = 3;
					//m_screen.rotatePacman(0);
					break;
				}
				
				break;
			}
			if (Event.type == sf::Event::Closed)
				m_window.close();

			
		}

	

		currentSec = clock.getElapsedTime().asSeconds();
		if (currentSec - lastSec2 > (1 / /*m_factor*/m_status.get_speed()))
		{
			bool collision = false;
			
			if (m_screen.move_pacman(prev_direction, direction, &m_status, &cookie_clock, collision))
				prev_direction = direction;
			if (collision)
			{
				audio_strike.play();
				direction = 5;
			}
				
			
			s_score.str("");
			s_lives.str("");
			s_score << "Score: " << m_status.get_score();//(m_screen.Get_Num_Devils()+1)*m_screen.get_eaten_cookies();
			s_lives << "Lives: " << m_status.get_lives();
			game_score.setString(s_score.str());
			lives.setString(s_lives.str());

			stopper = cookie_clock.getElapsedTime();

			if (stopper.asSeconds() >= 14.f && m_status.get_speed_cookie())
				m_status.set_speed(m_status.get_basic_speed());
			if (stopper.asSeconds() >= 7.f && m_status.get_slow_cookie())
				m_status.set_speed(m_status.get_basic_speed());

			// add this again
			//m_screen.move_random_devil(); // move random devils
			m_screen.move_smart_devil();
			lastSec2 = currentSec;
			
		}
		currentSec = clock.getElapsedTime().asSeconds()/*asMilliseconds()*/;
		if (currentSec - lastSec > (1 / (/*m_factor*/m_status.get_speed() /4)))
		{
			
			m_window.clear(sf::Color(0, 0, 0, 250));
			
			m_screen.draw(m_window); // print everything 
			m_window.draw(game_score);
			m_window.draw(lives);
			m_window.display();
			lastSec = currentSec;

			if (m_status.get_lives() == 0)
			{
				audio_strike.pause();
				audio_game_over.play();
				want_to_play = m_menu.lose(m_window);
				
				

				if (want_to_play)
				{
					m_status.set_life(3);
					m_status.set_score(0);
					m_screen.set_eaten_cookies(0);
					m_current_level = 1;
					m_screen.clear_all();
					load_level();
				}
			}
		}

	}
}

Controller::~Controller()
{
}
