#pragma once
#include "Dynamic.h"
#include "ResourceManager.h"
class Pacman:public Dynamic
{
public:
	//Pacman(int life,int score,int speed);
	Pacman( sf::Color color, sf::Vector2f position, char c);
	
	Pacman();

	void move(int direction); // 0- up, 1- down, 2- left, 3- right
	void draw(sf::RenderWindow & window);
	void  setScore(int score);
	void  getScore(int score) const;
	int getLife() const;
	int setLife() const;

	void set_position(sf::Vector2f new_position);

	void rotate(int rotation);
	void scale(float one, float two);


	~Pacman();


private:

	int m_life,
		m_score,
		m_speed;
};

