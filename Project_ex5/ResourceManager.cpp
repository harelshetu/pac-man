#include "ResourceManager.h"


ResourceManager::ResourceManager()
{

	if (!m_ptr_pacman.loadFromFile("pacman.png"))
		std::cout << "error cannot open picture" << std::endl;
	if (!m_ptr_wall.loadFromFile("wall.png"))
		std::cout << "error cannot open picture" << std::endl;
	if (!m_ptr_cookie.loadFromFile("cookie.png"))
		std::cout << "error cannot open picture" << std::endl;
	if (!m_ptr_devil.loadFromFile("devil.png"))
		std::cout << "error cannot open picture" << std::endl; 

	if (!m_font.loadFromFile("david.ttf"))
		std::cout << "cannot load font";


	if (!m_start_menu.loadFromFile("start_menu.jpg"))
		std::cout << "cannot load font";
	if (!m_game_over.loadFromFile("game_over.jpg"))
		std::cout << "cannot load font";
	if (!m_winner.loadFromFile("winner.jpg"))
		std::cout << "cannot load font";

	if (!m_play_button.loadFromFile("play_button.png"))
		std::cout << "cannot load font";
	if (!m_exit_button.loadFromFile("exit_button.png"))
		std::cout << "cannot load font";


	if (!m_start_game.loadFromFile("start_game.ogg"))
		std::cout << "cannot find audio";
	if (!m_eat_cookie.loadFromFile("eat_cookie.ogg"))
		std::cout << "cannot find audio";
	if (!m_strike.loadFromFile("strike.ogg"))
		std::cout << "cannot find audio";
	if (!m_defeat_audio.loadFromFile("end_game.ogg"))
		std::cout << "cannot find audio";

	



	/*sf::Texture m_ptr_pacman;
	sf::Texture m_ptr_wall;
	sf::Texture m_ptr_cookie;
	sf::Texture m_ptr_devil;

	if (!m_ptr_pacman.loadFromFile("pacman.png"))
		std::cout << "error cannot open picture" << std::endl;
	if (!m_ptr_wall.loadFromFile("wall.png"))
		std::cout << "error cannot open picture" << std::endl;
	if (!m_ptr_cookie.loadFromFile("cookie.png"))
		std::cout << "error cannot open picture" << std::endl;
	if (!m_ptr_devil.loadFromFile("devil.png"))
		std::cout << "error cannot open picture" << std::endl;
	if (!m_font.loadFromFile("david.ttf"))
		std::cout << "cannot load font";

	m_picture.push_back(m_ptr_pacman); //0
	m_picture.push_back(m_ptr_wall);  //1
	m_picture.push_back(m_ptr_cookie); //2
	m_picture.push_back(m_ptr_devil); // 3 */

	
}


ResourceManager & ResourceManager::instance()
{
	static ResourceManager m_instance;
	return m_instance;
}
const sf::SoundBuffer& ResourceManager::Get_Defeat_music() const
{
	return m_defeat_audio;
}


const sf::Texture& ResourceManager::GetPacmanPicture() const
{

	return m_ptr_pacman;
}
const sf::Texture& ResourceManager::GetWallPicture() const
{

	return m_ptr_wall;
}
const sf::Texture& ResourceManager::GetDevilPicture() const
{

	return m_ptr_devil;
}
const sf::Texture& ResourceManager::GetCookiePicture() const
{

	return m_ptr_cookie;
}

const sf::Font & ResourceManager::GetFont() const
{

	return m_font;
}

const sf::Texture & ResourceManager::Get_Texture(Picture_Type character) const
{

	switch (character)
	{
	case ResourceManager::pacman:
		return m_ptr_pacman;
		break;
	case ResourceManager::wall:
		return m_ptr_wall;
		break;
	case ResourceManager::cookie:
		return m_ptr_cookie;
		break;
	case ResourceManager::devil:
		return m_ptr_devil;
		break;
	case ResourceManager::start_menu:
		return m_start_menu;
		break;
	case ResourceManager::game_over:
		return m_game_over;
		break;
	case ResourceManager::winner:
		return m_winner;
		break;
	case ResourceManager::play_button:
		return m_play_button;
		break;
	}
	return m_exit_button;
	
}

const sf::Texture& ResourceManager::GetStartMenuPicture() const
{
	return m_start_menu;
}
const sf::Texture& ResourceManager::GetGameOverPicture() const
{
	return m_game_over;
}
const sf::Texture& ResourceManager::GetWinnerPicture() const
{
	return m_winner;
}
const sf::Texture& ResourceManager::GetPlayButtonPicture() const
{
	return m_play_button;
}
const sf::Texture& ResourceManager::GetExitButtonPicture() const
{
	return m_exit_button;
}

const sf::SoundBuffer& ResourceManager::Get_Start_game_music() const
{
	return m_start_game;

}
const sf::SoundBuffer& ResourceManager::Get_Eat_Cookie_music() const
{
	return m_eat_cookie;

}
const sf::SoundBuffer&ResourceManager::Get_Strike_music() const
{
	return m_strike;

}







ResourceManager::~ResourceManager()
{
}
