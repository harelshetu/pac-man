#include "Pacman.h"


/*
Pacman::Pacman(int life, int score, int speed)
	: m_life(life), m_score(score), m_speed(speed)
{
}
*/
Pacman::Pacman()
{
}



Pacman::Pacman( sf::Color color, sf::Vector2f position, char c)
	:Dynamic(color,position /*+ sf::Vector2f(12.5f, 12.5f)*/,c){
	//Object::set_position(position + sf::Vector2f(12.5f, 12.5f));
	//m_position += {12.5f, 12.5f};
	m_pic.setOrigin(12.5f, 12.5f);
}

void Pacman::set_position(sf::Vector2f new_position) {
	Object::set_position(new_position + sf::Vector2f(12.5f, 12.5f));
}

void Pacman::rotate(int rotation)
{
	m_pic.setRotation(rotation);
}

void Pacman::scale(float one, float two)
{
	m_pic.setScale(one, two);
}

void Pacman::draw(sf::RenderWindow & window)
{
	
	const sf::Texture& temp = ResourceManager::instance().GetPacmanPicture();
	
	m_pic.setTexture(temp/*ResourceManager::instance().GetPacmanPicture()*/);
	//m_pic.setPosition(m_position);
	m_pic.setColor(m_color);
	
	window.draw(m_pic);


}


void Pacman::move(int direction) // 0- up, 1- down, 2- left, 3- right
{
	//m_pic.move(5, 0);
	switch (direction)
	{
	case 0:
		m_position = { m_position.x  , m_position.y - 1 };
		break;
	case 1:
		m_position = { m_position.x  , m_position.y + 1 };
		break;
	case 2:
		m_position = { m_position.x - 1 , m_position.y };
		break;
	case 3:
		m_position = { m_position.x + 1 , m_position.y };
		break;
	}
	m_pic.setPosition(m_position);
}


Pacman::~Pacman()
{
}
