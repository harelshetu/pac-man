#include "Search_Algorithm.h"



Search_Algorithm::Search_Algorithm()
{
	m_direction = { UP,DOWN,LEFT,RIGHT };
	m_direction_name = { up,down,left,right };

}



int Search_Algorithm::bfs(sf::Vector2i start, sf::Vector2i target,
	int num_rows, int num_cols,
	std::vector< std::vector < std::unique_ptr <Static> > > *static_vec)
{
	bool found_destination = false,
		first = true;
	//	sf::Vector2i first_ver;
	std::vector <std::vector <Node> > map;
	sf::Vector2i save_start = start;
	m_destination = target;
	m_source = start;

	if (m_destination == m_source)
		return 5;

	for (int i = 0; i < num_cols; i++)
	{
		map.push_back({});

		for (int j = 0; j < num_rows; j++)
		{
			//map[i].push_back({});
			map.back().push_back({});
			map[i][j].discovered = false;

		}
	}

	m_que.clear();
	//save_start = { start.y,start.x };
	m_que.push_back(start);


	//m_que.push_back(m_source);
	while (!m_que.empty() && !found_destination)
	{
		save_start = m_que[0];
		sf::Vector2i save_double = m_que[0];
		m_que.erase(m_que.begin());


		for (int i = 0; i < 4 && !found_destination; i++)
		{

			found_destination = check_node(save_start, m_direction[i], static_vec, &map, num_rows, num_cols, i);
			/*if (found_destination == true)
			break;*/
			save_start = save_double;

		}
		map[save_double.y][save_double.x].discovered = true;
		//save_start = { save_start.y,save_start.x };
	}


	for (int i = 0; i < num_cols; i++)
	{
		for (int j = 0; j < num_rows; j++)
		{
			map[i][j].discovered = false;

		}
	}

	if (m_que.size() == 0)
		return 5;

	sf::Vector2i backwbackwards_vertex = m_que.at(m_que.size() - 1);
	//backwbackwards_vertex = { backwbackwards_vertex.y,backwbackwards_vertex.x };
	direction_type path;

	while ((backwbackwards_vertex != m_source))
	{

		if (map[backwbackwards_vertex.y][backwbackwards_vertex.x]._direction == LEFT)
		{
			backwbackwards_vertex.x -= 1;
			path = left;
		}
		else if (map[backwbackwards_vertex.y][backwbackwards_vertex.x]._direction == RIGHT)
		{
			backwbackwards_vertex.x += 1;
			path = right;

		}
		else if (map[backwbackwards_vertex.y][backwbackwards_vertex.x]._direction == UP)
		{
			backwbackwards_vertex.y -= 1;
			path = up;

		}
		else
		{
			backwbackwards_vertex.y += 1;
			path = down;
		}

		sf::Vector2i devil_path = map[backwbackwards_vertex.y][backwbackwards_vertex.x]._direction;

		//backwbackwards_vertex += map[backwbackwards_vertex.x][backwbackwards_vertex.y]._direction;
	}

	//map[backwbackwards_vertex.y][backwbackwards_vertex.x]._direction = map[backwbackwards_vertex.y][backwbackwards_vertex.x]._direction*-1;

	/*sf::Vector2i res = { map[backwbackwards_vertex.x][backwbackwards_vertex.y]._direction.x *-1,
	map[backwbackwards_vertex.x][backwbackwards_vertex.y]._direction.y*-1 };*/

	/*if (backwbackwards_vertex == LEFT)
	return 2;
	if (backwbackwards_vertex == RIGHT)
	return 3;
	if (backwbackwards_vertex == UP)
	return 0;*/
	//0-up 1-down 2-left 3-right

	if (path == left)
		return 3;
	if (path == right)
		return 2;
	if (path == up)
		return 1;

	return 0;
}

bool Search_Algorithm::check_node(sf::Vector2i &vertex, sf::Vector2i direction_1,
	std::vector< std::vector < std::unique_ptr <Static> > > *static_vec,
	std::vector <std::vector <Node> > *map_ptr, int num_rows, int num_cols, int index)
{
	sf::Vector2i stat_vertex = vertex;
	for (int i = 0; i< m_direction_name.size(); i++)
		if (m_direction[i] == direction_1)
		{
			switch (m_direction_name[i])
			{
			case left:
				vertex.x = vertex.x - 1;//y
										//stat_vertex.y = stat_vertex.y - 1;
				break;
			case right:
				vertex.x = vertex.x + 1;
				//stat_vertex.y = stat_vertex.y + 1;
				break;//y
			case up:
				vertex.y = vertex.y - 1; //x
										 //stat_vertex.x = stat_vertex.x - 1;
				break;
			case down:
				vertex.y = vertex.y + 1;
				//stat_vertex.x = stat_vertex.x + 1;//x
				break;
			}
		}

	//sf::Vector2i res = vertex + direction_1;
	if (vertex.y > num_cols || vertex.y <0 || vertex.x > num_rows || vertex.x < 0)
		return false;

	if (static_vec->at(vertex.x).at(vertex.y) == nullptr || static_vec->at(vertex.x).at(vertex.y) != nullptr)
	{
		if (static_vec->at(vertex.x).at(vertex.y) != nullptr)
		{
			if (static_vec->at(vertex.x).at(vertex.y)->get_type() != '#'
				&& static_vec->at(vertex.x).at(vertex.y)->get_type() != 'E'
				&& static_vec->at(vertex.x).at(vertex.y)->get_type() != 'D'
				&& map_ptr->at(vertex.y).at(vertex.x).discovered == false)
			{
				map_ptr->at(vertex.y).at(vertex.x).discovered = true;
				map_ptr->at(vertex.y).at(vertex.x)._direction = direction_1 * -1;
				//vertex = stat_vertex;
				m_que.push_back(vertex);
				sf::Vector2i opposite = { vertex.y,vertex.x };


				if (m_destination == opposite)
					return true;

			}
		}

		if ((static_vec->at(vertex.x).at(vertex.y) == nullptr))
		{
			if (map_ptr->at(vertex.y).at(vertex.x).discovered == false)
			{
				map_ptr->at(vertex.y).at(vertex.x).discovered = true;
				map_ptr->at(vertex.y).at(vertex.x)._direction = direction_1 * -1;
				//vertex = stat_vertex;
				m_que.push_back(vertex);
				sf::Vector2i opposite = { vertex.y,vertex.x };


				if (m_destination == opposite)
					return true;
			}

		}
	}


	return false;
}




Search_Algorithm::~Search_Algorithm()
{
}



