#include "Menu.h"



Menu::Menu()
{
}

bool Menu::main_loop(sf::RenderWindow & window, sf::Vector2f play_position, sf::Vector2f exit_position)
{
	bool play = false;

	window.create(sf::VideoMode::VideoMode(600, 600), "Pacman");

	while (window.isOpen())
	{
		window.clear();
		
		window.draw(m_menu);
		draw_play_button(window, play_position);
		draw_exit_button(window, exit_position);
		
		sf::Event Event;
		while (window.pollEvent(Event))
		{
			switch (Event.type)
			{

			case sf::Event::MouseButtonPressed:
				if (Event.mouseButton.button == sf::Mouse::Left)
				{
					int x = Event.mouseButton.x,
						y = Event.mouseButton.y;
					if (x > play_position.x && x < (play_position.x+150) && y>(play_position.y+5) &&
						y < (play_position.y + 75))
					{
						window.close();
						play = true;
					}
					if (x > exit_position.x && x < (exit_position.x+150) && y> (exit_position.y+15) && 
						y < (exit_position.y + 95))
					{
						window.close();
						play = false;
					}
				}
				break;
			case sf::Event::MouseMoved:
				int x = Event.mouseMove.x,
					y = Event.mouseMove.y;

				if (x > play_position.x && x < (play_position.x + 150) && y>(play_position.y + 5) &&
					y < (play_position.y + 75))
				{
					draw_play_button(window, { play_position.x-3, play_position.y+3 });
					window.display();
				}
				if (x > exit_position.x && x < (exit_position.x + 150) && y>(exit_position.y + 15) &&
					y < (exit_position.y + 95))
				{
					draw_exit_button(window, { exit_position.x-3, exit_position.y+3 });
					window.display();
				}
				break;
			}
			
			//window.display();
		}
		
		window.display();

	}
	return play;
}

bool Menu::start(sf::RenderWindow & window)
{
	bool play = false;
	const sf::Texture& temp = ResourceManager::instance().GetStartMenuPicture();
	m_menu.setTexture(temp);
	play = main_loop(window, { 200,220 }, { 450,210 });

	return play;
}

bool Menu::win(sf::RenderWindow & window)
{
	bool play = false;
	const sf::Texture& temp = ResourceManager::instance().GetWinnerPicture();
	m_menu.setTexture(temp);
	play = main_loop(window, { 200,350 }, { 450,355 });

	return play;
}

bool Menu::lose(sf::RenderWindow & window)
{
	bool play = false;
	const sf::Texture& temp = ResourceManager::instance().GetGameOverPicture();
	m_menu.setTexture(temp);
	play = main_loop(window, { 125,450 }, { 395,440 });

	return play;
}

void Menu::draw_play_button(sf::RenderWindow & window, sf::Vector2f position)
{
	const sf::Texture& temp = ResourceManager::instance().GetPlayButtonPicture();
	m_play_button.setTexture(temp);
	m_play_button.setPosition(position);
	window.draw(m_play_button);
}

void Menu::draw_exit_button(sf::RenderWindow & window, sf::Vector2f position)
{
	const sf::Texture& temp = ResourceManager::instance().GetExitButtonPicture();
	m_exit_button.setTexture(temp);
	m_exit_button.setPosition(position);
	window.draw(m_exit_button);
}

Menu::~Menu()
{
}

