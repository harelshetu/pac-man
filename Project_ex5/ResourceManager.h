#pragma once
#include <SFML/Graphics.hpp>
#include <SFML\Audio.hpp>
#include <iostream>
#include <vector>


class ResourceManager
{
public:


	static ResourceManager & instance();
	const sf::Texture& GetPacmanPicture() const;
	const sf::Texture& GetCookiePicture() const;
	const sf::Texture& GetWallPicture() const;
	const sf::Texture& GetDevilPicture() const;
	const sf::Font & GetFont() const;
	
	const sf::Texture& GetStartMenuPicture() const;
	const sf::Texture& GetGameOverPicture() const;
	const sf::Texture& GetWinnerPicture() const;
	const sf::Texture& GetPlayButtonPicture() const;
	const sf::Texture& GetExitButtonPicture() const;
	const sf::SoundBuffer& Get_Start_game_music() const;
	const sf::SoundBuffer& Get_Eat_Cookie_music() const;
	const sf::SoundBuffer& Get_Strike_music() const;
	const sf::SoundBuffer& Get_Defeat_music() const;

	
	enum Picture_Type {pacman,
			   wall,
			   cookie,
			   devil, 
			   start_menu,
			   game_over,
			   winner,
			   play_button,
			   exit_button
				};

	enum Audio_Type {
					open_menu,
					start_game,
					eat_cookie,
					strike,		
	};

	const sf::Texture &Get_Texture(Picture_Type character) const;


	~ResourceManager();



private:
	/*sf::Sprite m_pic_pacman;
	sf::Sprite m_pic_cookie;
	sf::Sprite m_pic_wall;
	sf::Sprite m_pic_devil;*/

	sf::Texture m_ptr_pacman;
	sf::Texture m_ptr_wall;
	sf::Texture m_ptr_cookie;
	sf::Texture m_ptr_devil;
	sf::Texture m_start_menu,
				m_game_over,
				m_winner,
				m_play_button,
				m_exit_button;
	sf::SoundBuffer m_open_menu,
					m_start_game,
					m_eat_cookie,
					m_strike,
					m_defeat_audio;

	

	std::vector<sf::Texture> m_picture;
	sf::Font m_font;
	ResourceManager(const ResourceManager&) = delete;
	ResourceManager();

};

