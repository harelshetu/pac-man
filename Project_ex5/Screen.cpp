#include "Screen.h"



Screen::Screen()
	:m_eaten_cookies(0)
{
}

void Screen::set_eaten_cookies(int num)
{
	m_eaten_cookies = num;
}

void Screen::rotatePacman(int direction)
{
	//m_pacman->rotate(direction);
	switch (direction)
	{
	case 0:
		m_pacman->rotate(270);
		break;
	case 1:
		m_pacman->rotate(90);
		break;
	case 2:
		m_pacman->rotate(180);
		break;
	case 3:
		m_pacman->rotate(0);
		break;
	}
	
}

int Screen::get_stage_cookies() const
{
	return m_level.get_num_cookies();
}
int Screen::get_eaten_cookies() const
{
	return m_eaten_cookies;
}

int Screen::get_rows_num() const
{
	return m_level.get_rows_num();
}
int Screen::get_cols_num() const
{
	return m_level.get_cols_num();
}

void Screen::load_level(int level)
{
	m_eaten_cookies = 0;
	std::string level_name = "level" + std::to_string(level)+".txt";
	m_level.load_level(level_name, m_pacman, &m_static, &m_devil);
}

bool Screen::pacman_cookie_collision(sf::Vector2f next_position, int direction, Player_Status * stat, sf::Clock *cookie_clock)
{
	unsigned int x = (next_position.x) / 35,
		y = (next_position.y) / 35;
	int width = m_level.get_cols_num(),
		height = m_level.get_rows_num();
	

	if (m_static[x][y] != nullptr)
	{
		char type = m_static[x][y]->get_type();
		if (type == '*' || type == 'I' || type == 'K')
		{
			

			sf::Vector2f v1 = get_center_of_sprite(m_static[x][y]->get_pic());
			sf::Vector2f v2 = m_pacman->get_pic().getPosition();
			if (distance(v1, v2) < (get_radius_of_pacman()) /2)
			{
				m_static[x][y] = nullptr;
				if (type == 'I')
				{
					cookie_clock->restart();
					stat->set_speed_cookie(true);
					stat->set_speed(stat->get_speed() + (stat->get_speed()*0.2));
				}
				if (type == '*')
				{
					cookie_clock->restart();
					stat->set_slow_cookie(true);
					stat->set_speed(stat->get_speed() - (stat->get_speed()*0.1));
				}
				return true;
			}
		}
	}
	return false;

}

double Screen::distance(sf::Vector2f v1, sf::Vector2f v2)
{
	return std::sqrt(std::pow((v1.x - v2.x), 2)
		+ std::pow((v1.y - v2.y), 2));
}

bool Screen::pacman_wall_collision(int x, int y)
{
	if(m_static[x][y]->get_pic().getGlobalBounds().intersects(m_pacman->get_pic().getGlobalBounds()) || 
		m_pacman->get_pic().getGlobalBounds().intersects(m_static[x][y]->get_pic().getGlobalBounds() ))
		return true;

	return false;		
}

bool Screen::devil_wall_collision(int x, int y, int devil_index)
{
	if (m_static[x][y]->get_pic().getGlobalBounds().intersects(m_devil[devil_index]->get_pic().getGlobalBounds()) ||
		m_devil[devil_index]->get_pic().getGlobalBounds().intersects(m_static[x][y]->get_pic().getGlobalBounds()))
		return true;

	return false;
}


bool Screen::wall_collision(sf::Vector2f next_position, int direction, bool pacman, int devil_index) // bool pacman or devil
{
	unsigned int x = (next_position.x) / 35,
		y = (next_position.y) / 35;
	if (direction == 1) //up
		y++;
	if (direction == 3) // right
		x++;

	int width = m_level.get_cols_num(),
		height = m_level.get_rows_num();

	for (int i = x - 1; i <= x + 1 && i!=-1 && i!=height; i++)
	{
		if (m_static[i][y] != nullptr)
		{
			char type = m_static[i][y]->get_type();
			if (type == '#' || type == 'E' || type == 'D')
				if (pacman)
				{
					if (pacman_wall_collision(i, y))
						return true;
				}
				else // if devil
					if (devil_wall_collision(i, y, devil_index))
						return true;
		}
	}
	for (int i = y - 1; i <= y + 1 && i != -1 && i != width; i++)
	{
		if (m_static[x][i] != nullptr)
		{
			char type = m_static[x][i]->get_type();
			if (type == '#' || type == 'E' || type == 'D')
			{	
				if (pacman)
				{
					if (pacman_wall_collision(x, i))
						return true;
				}
				else // if devil
					if (devil_wall_collision(x, i, devil_index))
						return true;
			}	
		}
	}
	return false;
}


bool Screen::move_pacman(int prev_direction, int direction, Player_Status * stat, sf::Clock *cookie_clock, bool &collision) // 0- up, 1- down, 2- left, 3- right
{
	bool user_direction = false;
	sf::Vector2f prev_pos = m_pacman->get_position();
	//sf::Sound audio_eat_cookie;

	m_pacman->move(direction);

	if (wall_collision(sf::Vector2f(m_pacman->get_pic().getGlobalBounds().left, m_pacman->get_pic().getGlobalBounds().top), direction, true, 0))
	{
		m_pacman->set_position(prev_pos - sf::Vector2f(12.5f, 12.5f));
		if (prev_direction != direction)
		{
			m_pacman->move(prev_direction);
			if (wall_collision(sf::Vector2f(m_pacman->get_pic().getGlobalBounds().left, m_pacman->get_pic().getGlobalBounds().top), prev_direction, true, 0))
				m_pacman->set_position(prev_pos - sf::Vector2f(12.5f, 12.5f));
		}
		
	}
	else
	{
		rotatePacman(direction);
		user_direction = true;
	}
	if (pacman_cookie_collision(sf::Vector2f(m_pacman->get_pic().getGlobalBounds().left, m_pacman->get_pic().getGlobalBounds().top), direction,stat, cookie_clock))
	{
		m_eaten_cookies++;
		int score = stat->get_score();
		stat->set_score(score + ((m_devil.size() + 1) * 2));
		m_eat_cookie_audio.setBuffer(ResourceManager::instance().Get_Eat_Cookie_music());
		m_eat_cookie_audio.play();
	}
	for (int i = 0; i < m_devil.size(); i++)
	{
		if (m_devil[i] != nullptr)
			if (pacman_devil_collision(m_pacman->get_pic(), m_devil[i]->get_pic()))
			{
				m_pacman->set_position(m_pacman->get_first_position());
				m_devil[i]->set_position(m_devil[i]->get_first_position());
				stat->set_life(stat->get_lives() - 1);
				collision = true;
				m_pacman->rotate(0);
			}
	}
	return user_direction;
}

void Screen::move_smart_devil()
{
	int smart_devil_x = m_devil[0]->get_position().x / 35,
		smart_devil_y = m_devil[0]->get_position().y / 35;

	int pacman_x = m_pacman->get_position().x/35,
		pacman_y = m_pacman->get_position().y / 35;



	m_devil_direction = m_algorithm.bfs({ smart_devil_x,smart_devil_y }, { pacman_y ,pacman_x },  
		m_level.get_rows_num(), m_level.get_cols_num(), &m_static);
	if (m_devil_direction == 5)
		m_devil_direction = m_devil_prev_direction;

	sf::Vector2f prev_pos = m_devil[0]->get_position();
	m_devil[0]->move(m_devil_direction);
	if (wall_collision(m_devil[0]->get_position(), m_devil_direction, false, 0))
	{
		m_devil[0]->set_position(prev_pos);
		if (m_devil_direction == m_devil_prev_direction)
		{
			if (m_devil_direction == 1 || m_devil_direction == 0)
			m_devil_prev_direction = 2;
			if (m_devil_direction ==  2 || m_devil_direction == 3)
				m_devil_prev_direction = 1;
		}
		prev_pos = m_devil[0]->get_position();
		m_devil[0]->move(m_devil_prev_direction);
		if (wall_collision(m_devil[0]->get_position(), m_devil_prev_direction, false, 0))
		{
			m_devil[0]->set_position(prev_pos);
			m_devil_direction = m_devil_prev_direction;
		}
		
	}
	else
		if (m_devil_direction != 5)
			m_devil_prev_direction = m_devil_direction;
}

void Screen::move_random_devil()
{
	//for (unsigned int i = 0; i < m_devil.size(); i++)
	//{
	if (m_devil[0] != nullptr)
	{
		sf::Vector2f prev_pos = m_devil[0]->get_position();
		m_devil[0]->move(m_devil_direction);
		if (wall_collision(m_devil[0]->get_position(), m_devil_direction, false, 0))
		{
			m_devil[0]->set_position(prev_pos);
			m_devil_direction = rand() % 4;
		}


		//if (pacman_devil_collision(m_pacman->get_pic(), m_devil[0]->get_pic()))
		//{
		//	m_pacman->set_position(m_pacman->get_first_position());
		//	m_devil[0]->set_position(m_devil[0]->get_first_position());
		//}
	}
	//}
}

bool Screen::pacman_devil_collision(sf::Sprite pacman, sf::Sprite devil)
{
	if (pacman.getGlobalBounds().intersects(devil.getGlobalBounds()) ||
		devil.getGlobalBounds().intersects(pacman.getGlobalBounds()))
		return true;

	return false;
}

void Screen::draw(sf::RenderWindow & window)
{
	////////////////////////////////////////// need to check if pacman and vectors are not empty

	
	
	for (unsigned int i = 0; i < m_static.size(); i++)
		for (unsigned int j = 0; j < m_static[0].size(); j++)
			if (m_static[i][j] != nullptr)
				m_static[i][j]->draw(window);
		
	m_pacman->draw(window);

	for (unsigned int i = 0; i < m_devil.size(); i++)
		if (m_devil[i] != nullptr)
			m_devil[i]->draw(window);
}

sf::Vector2f Screen::get_center_of_sprite(sf::Sprite picture)
{
	sf::Vector2f center;
	center.x = picture.getPosition().x + picture.getGlobalBounds().width / 2;
	center.y = picture.getPosition().y + picture.getGlobalBounds().width / 2;
	return center;
}

double Screen::get_radius_of_pacman()
{
	double width = m_pacman->get_pic().getGlobalBounds().width;
	return width;
}

void Screen::set_devil_direction(int direction)
{
	m_devil_direction = direction;
}



void Screen::clear_all()
{
	for(int i=0; i< m_static.size(); i++)
		for (int j = 0; j< m_static[0].size(); j++)
			m_static[i][j] = nullptr;
	for (int i = 0; i < m_devil.size(); i++)
		m_devil[i] = nullptr;

	m_devil.resize(0);
	
	
	
	//m_pacman.reset();
}

int Screen::Get_Num_Devils() const
{

	return m_devil.size();
}








Screen::~Screen()
{
}
