#include "Cookie.h"

Cookie::Cookie(sf::Color color, sf::Vector2f position, char c)
	: Static(color,position,c)
{
}



void Cookie::draw(sf::RenderWindow & window)
{
	
	const sf::Texture& temp = ResourceManager::instance().GetCookiePicture();
	m_pic.setTexture(temp);
	m_pic.setPosition(m_position);
	m_pic.setColor(m_color);
	window.draw(m_pic);
}

//void Cookie::draw(sf::RenderWindow & window)   // upload the image 
//{
//	sf::Texture ptr_pic;
//	sf::Sprite pacman_pic;
//
//	if (!ptr_pic.loadFromFile("wall.png"))
//		std::cout << "error cannot open picture" << std::endl;
//	pacman_pic.setTexture(ptr_pic);
//	pacman_pic.setPosition(m_position);
//	pacman_pic.setColor(m_color);
//	window.draw(pacman_pic);
//}

Cookie::~Cookie()
{
}
